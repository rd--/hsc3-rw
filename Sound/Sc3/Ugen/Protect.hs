{- | Functions to re-write assigned node identifiers at Ugen graphs.
  Used carefully it allows for composition of sub-graphs with psuedo-random nodes.
-}
module Sound.Sc3.Ugen.Protect where

import Sound.Sc3.Common.Uid {- hsc3 -}
import Sound.Sc3.Ugen.Types {- hsc3 -}
import Sound.Sc3.Ugen.Util {- hsc3 -}

-- | The set of all 'UgenId' at a 'Ugen' graph
ugen_id_set :: Ugen -> [UgenId]
ugen_id_set =
  let f u = case u of
        Primitive_U p -> [ugenId p]
        _ -> []
  in ugenFoldr ((++) . f) []

-- | Replace Uid /i/ at /z/ with 'resolveID' of /(e,i)/.
ugenid_edit :: ID a => a -> UgenId -> UgenId
ugenid_edit e z =
  case z of
    NoId -> NoId
    Uid i -> Uid (resolveID (e, i))

-- | Apply /f/ to the name of 'Control'
control_edit_name :: (String -> String) -> Control -> Control
control_edit_name f c = c {controlName = f (controlName c)}

ugen_graph_rw_ctl :: (String -> String) -> Ugen -> Ugen
ugen_graph_rw_ctl nm_f =
  let rw_f u = case u of
        Control_U c -> Control_U (control_edit_name nm_f c)
        _ -> u
  in ugenTraverse (const False) rw_f

ugen_graph_rw_uid :: ID z => z -> Ugen -> Ugen
ugen_graph_rw_uid z =
  let rw_f u = case u of
        Primitive_U p -> Primitive_U (p {ugenId = ugenid_edit z (ugenId p)})
        _ -> u
  in ugenTraverse (const False) rw_f

-- | 'ugenTraverse' running 'control_name_edit' and 'ugenid_edit'
ugen_graph_rw_ctl_and_uid :: ID z => (String -> String) -> z -> Ugen -> Ugen
ugen_graph_rw_ctl_and_uid nm_f z =
  let rw_f u = case u of
        Control_U c -> Control_U (control_edit_name nm_f c)
        Primitive_U p -> Primitive_U (p {ugenId = ugenid_edit z (ugenId p)})
        _ -> u
  in ugenTraverse (const False) rw_f

{- | 'ugenid_edit' of /e/ at all 'Primitive_U' of /u/.
     The halting rule allows delimiting the re-writer.
     It would be better if Ugen had a notion of a sub-graph,
     and the protect rule applied only to sub-graphs

> import Sound.Sc3 {\- hsc3 -\}
> ugenIds (uprotect (const False) 'β' (whiteNoise 'α' AR)) == [Uid (resolveID ('β',resolveID 'α'))]
-}
uprotect :: ID a => (Ugen -> Bool) -> a -> Ugen -> Ugen
uprotect halt_f z =
  let map_f u = case u of
        Primitive_U p -> Primitive_U (p {ugenId = ugenid_edit z (ugenId p)})
        _ -> u
  in ugenTraverse halt_f map_f

-- | Variant with no halting rule.
uprotect_all :: ID a => a -> Ugen -> Ugen
uprotect_all = uprotect (const False)

{- | Variant of 'uprotect' with subsequent identifiers derived by
incrementing initial identifier.
-}
uprotect_seq :: ID a => (Ugen -> Bool) -> a -> [Ugen] -> [Ugen]
uprotect_seq halt_f e =
  let n = map (+ resolveID e) [1 ..]
  in zipWith (uprotect halt_f) n

-- | Make /n/ instances of 'Ugen' with protected identifiers.
uclone_seq :: ID a => (Ugen -> Bool) -> a -> Int -> Ugen -> [Ugen]
uclone_seq halt_f e n = uprotect_seq halt_f e . replicate n

-- | 'mce' of 'uclone_seq'.
uclone :: ID a => (Ugen -> Bool) -> a -> Int -> Ugen -> Ugen
uclone halt_f e n = mce . uclone_seq halt_f e n

-- | Variant with no halting rule.
uclone_all :: ID a => a -> Int -> Ugen -> Ugen
uclone_all = uclone (const False)

-- | Left to right Ugen function composition with 'UgenId' protection.
ucompose :: ID a => (Ugen -> Bool) -> a -> [Ugen -> Ugen] -> Ugen -> Ugen
ucompose halt_f e xs =
  let go [] u = u
      go ((f, k) : f') u = go f' (uprotect halt_f k (f u))
  in go (zip xs [resolveID e ..])

-- | Make /n/ sequential instances of `f' with protected Ids.
useq :: ID a => (Ugen -> Bool) -> a -> Int -> (Ugen -> Ugen) -> Ugen -> Ugen
useq halt_f e n f = ucompose halt_f e (replicate n f)

-- | Variant with no halting rule.
useq_all :: ID a => a -> Int -> (Ugen -> Ugen) -> Ugen -> Ugen
useq_all = useq (const False)
