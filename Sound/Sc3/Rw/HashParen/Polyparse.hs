module Sound.Sc3.Rw.HashParen.Polyparse where

import Data.Function {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Text.ParserCombinators.Poly.State as P {- polyparse -}

type Binding = (String, String)
type Name_Supply = [String]

type HP_Char = (Char, Maybe Int)
type HP_String = [HP_Char]

type ST = (Int, [Int])
type HP = P.Parser ST Char

hp_st :: ST
hp_st = (0, [])

safe_head :: [a] -> Maybe a
safe_head l =
  case l of
    [] -> Nothing
    e : _ -> Just e

-- | Only count parens in #().
hp_next :: HP (Char, Maybe Int)
hp_next = do
  let stPut st = P.stUpdate (const st)
  c <- P.next
  (n, h) <- P.stGet
  case c of
    '#' -> stPut (n, n : h) >> return (c, Just n)
    '(' -> stPut (if null h then (n, h) else (n + 1, h)) >> return (c, safe_head h)
    ')' ->
      let n' = n - 1
          (st', e) = case h of
            [] -> ((n, []), Nothing)
            x : h' ->
              if x == n'
                then ((n', h'), Just x)
                else ((n', h), safe_head h)
      in stPut st' >> return (c, e)
    _ -> return (c, safe_head h)

{- | Hp hash paren

>>> P.runParser hp_hash_paren hp_st "r <- #(a)"
(Right [('r',Nothing),(' ',Nothing),('<',Nothing),('-',Nothing),(' ',Nothing),('#',Just 0),('(',Just 0),('a',Just 0),(')',Just 0)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "#(a (b)) (c (d))"
(Right [('#',Just 0),('(',Just 0),('a',Just 0),(' ',Just 0),('(',Just 0),('b',Just 0),(')',Just 0),(')',Just 0),(' ',Nothing),('(',Nothing),('c',Nothing),(' ',Nothing),('(',Nothing),('d',Nothing),(')',Nothing),(')',Nothing)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "#(a (b) (c (d)))"
(Right [('#',Just 0),('(',Just 0),('a',Just 0),(' ',Just 0),('(',Just 0),('b',Just 0),(')',Just 0),(' ',Just 0),('(',Just 0),('c',Just 0),(' ',Just 0),('(',Just 0),('d',Just 0),(')',Just 0),(')',Just 0),(')',Just 0)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "#a"
(Right [('#',Just 0),('a',Just 0)],(0,[0]),"")

>>> P.runParser hp_hash_paren hp_st "a"
(Right [('a',Nothing)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "c <- f #(a) #(b c) d"
(Right [('c',Nothing),(' ',Nothing),('<',Nothing),('-',Nothing),(' ',Nothing),('f',Nothing),(' ',Nothing),('#',Just 0),('(',Just 0),('a',Just 0),(')',Just 0),(' ',Nothing),('#',Just 0),('(',Just 0),('b',Just 0),(' ',Just 0),('c',Just 0),(')',Just 0),(' ',Nothing),('d',Nothing)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "c <- f #(a) #(b #(c)) d"
(Right [('c',Nothing),(' ',Nothing),('<',Nothing),('-',Nothing),(' ',Nothing),('f',Nothing),(' ',Nothing),('#',Just 0),('(',Just 0),('a',Just 0),(')',Just 0),(' ',Nothing),('#',Just 0),('(',Just 0),('b',Just 0),(' ',Just 0),('#',Just 1),('(',Just 1),('c',Just 1),(')',Just 1),(')',Just 0),(' ',Nothing),('d',Nothing)],(0,[]),"")

>>> P.runParser hp_hash_paren hp_st "c <- f #(a) #(b #(c #(d e) f) #(g)) #(h) i"
(Right [('c',Nothing),(' ',Nothing),('<',Nothing),('-',Nothing),(' ',Nothing),('f',Nothing),(' ',Nothing),('#',Just 0),('(',Just 0),('a',Just 0),(')',Just 0),(' ',Nothing),('#',Just 0),('(',Just 0),('b',Just 0),(' ',Just 0),('#',Just 1),('(',Just 1),('c',Just 1),(' ',Just 1),('#',Just 2),('(',Just 2),('d',Just 2),(' ',Just 2),('e',Just 2),(')',Just 2),(' ',Just 1),('f',Just 1),(')',Just 1),(' ',Just 0),('#',Just 1),('(',Just 1),('g',Just 1),(')',Just 1),(')',Just 0),(' ',Nothing),('#',Just 0),('(',Just 0),('h',Just 0),(')',Just 0),(' ',Nothing),('i',Nothing)],(0,[]),"")
-}
hp_hash_paren :: HP HP_String
hp_hash_paren = P.many1 hp_next

{- | Hp parse

>>> hp_parse "c <- f #(a) #(b #(c #(d e) f) g) h"
[('c',Nothing),(' ',Nothing),('<',Nothing),('-',Nothing),(' ',Nothing),('f',Nothing),(' ',Nothing),('#',Just 0),('(',Just 0),('a',Just 0),(')',Just 0),(' ',Nothing),('#',Just 0),('(',Just 0),('b',Just 0),(' ',Just 0),('#',Just 1),('(',Just 1),('c',Just 1),(' ',Just 1),('#',Just 2),('(',Just 2),('d',Just 2),(' ',Just 2),('e',Just 2),(')',Just 2),(' ',Just 1),('f',Just 1),(')',Just 1),(' ',Just 0),('g',Just 0),(')',Just 0),(' ',Nothing),('h',Nothing)]
-}
hp_parse :: String -> HP_String
hp_parse s =
  case P.runParser hp_hash_paren hp_st s of
    (Right r, (0, []), []) -> r
    _ -> error "hp_parse"

{- | Left biased 'max' variant.

>>> max_by last "cat" "mouse"
"cat"

>>> max_by last "aa" "za"
"aa"
-}
max_by :: Ord a => (t -> a) -> t -> t -> t
max_by f p q = if f q > f p then q else p

{- | Replace first

>>> replace_first 1 (-1) [-2,1,0,1]
[-2,-1,0,1]
-}
replace_first :: Eq a => a -> a -> [a] -> [a]
replace_first p q =
  let rec r l = case l of
        [] -> reverse r
        e : l' -> if e == p then reverse (q : r) ++ l' else rec (e : r) l'
  in rec []

{- | Un hash paren

>>> un_hash_paren "#(a)"
"a"

>>> un_hash_paren "b"
"b"
-}
un_hash_paren :: String -> String
un_hash_paren s =
  let f = reverse . drop 1 . reverse
  in case s of
      '#' : '(' : s' -> f s'
      _ -> s

hp_next_binding :: Name_Supply -> HP_String -> Maybe (Name_Supply, Binding, HP_String)
hp_next_binding n s =
  if null s || all ((== Nothing) . snd) s
    then Nothing
    else
      let nm : n' = n
          s' = groupBy ((==) `on` snd) s
          e = foldl1 (max_by (fromMaybe (-1) . snd . head)) s'
          x = fromJust (snd (head e)) - 1
          x' = if x >= 0 then Just x else Nothing
          s'' = replace_first e (map (\c -> (c, x')) nm) s'
      in Just (n', (nm, un_hash_paren (map fst e)), concat s'')

hp_print :: HP_String -> String
hp_print = map fst
