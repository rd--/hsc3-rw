-- | Rewrite expressions and modules attaching tags to numeric literals.
module Sound.Sc3.Rw.Tag where

import qualified Control.Monad.Trans.State as T {- transformers -}
import qualified Data.Functor.Identity as T {- transformers -}

import qualified Data.Generics as G {- syb -}

import qualified Language.Haskell.Exts as E {- haskell-src-exts -}

-- | Expression with source location information
type Exp = E.Exp E.SrcSpanInfo

{- | Make 'Var' 'Exp' for 'String'. -}
mk_var :: String -> Exp
mk_var = E.Var E.noSrcSpan . E.UnQual E.noSrcSpan . E.Ident E.noSrcSpan

{- | Make 'String' 'Lit'. -}
mk_str_lit :: String -> Exp
mk_str_lit s = E.Lit E.noSrcSpan (E.String E.noSrcSpan s s)

-- | Make 'Int' 'Lit'.
mk_int_lit :: Integral n => n -> Exp
mk_int_lit n = E.Lit E.noSrcSpan (E.Int E.noSrcSpan (toInteger n) "...")

-- | Make 'Frac' 'Lit'.
mk_frac_lit :: Real n => n -> Exp
mk_frac_lit n = E.Lit E.noSrcSpan (E.Frac E.noSrcSpan (toRational n) "...")

-- | Numeric literal at 'Exp' else 'Nothing'
exp_num_lit :: Exp -> Maybe (E.Literal E.SrcSpanInfo)
exp_num_lit e =
  case e of
    E.Lit _ (E.Int l n s) -> Just (E.Int l n s)
    E.Lit _ (E.Frac l n s) -> Just (E.Frac l n s)
    _ -> Nothing

-- | Tag an 'Exp' with 'String'.
tag_exp :: String -> Exp -> Exp
tag_exp k e = E.App E.noSrcSpan (E.App E.noSrcSpan (mk_var "tag") (mk_str_lit k)) e

{- | Apply /f/ at 'Exp' /e/ if it is tagged, else /g/.
     If the tag is within a 'Paren' it is discarded.
-}
at_tagged :: (String -> Exp -> Exp) -> (Exp -> Exp) -> Exp -> Exp
at_tagged f g e =
  case e of
    E.Paren l e' -> at_tagged f (E.Paren l . g) e'
    E.App _ (E.App _ (E.Var _ (E.UnQual _ (E.Ident _ "tag"))) (E.Lit _ (E.String _ k _))) e' -> f k e'
    _ -> g e

{- | Inverse of 'tag_exp'.

>>> let z = mk_int_lit (0::Integer)
>>> untag_exp (E.Paren E.noSrcSpan (tag_exp "c1" z)) == z
True
-}
untag_exp :: Exp -> Exp
untag_exp = at_tagged (\_ e -> e) id

-- | Empty source location.
nil_src_loc :: E.SrcLoc
nil_src_loc = E.SrcLoc "" 0 0

-- | Create an E.XTag node.  haskell-src-exts very conveniently has support for this.
mk_span_id :: String -> [Exp] -> Exp
mk_span_id k =
  let nm = E.XName E.noSrcSpan "span"
      cl_a = E.XAttr E.noSrcSpan (E.XName E.noSrcSpan "class") (mk_str_lit "numeric-literal")
      id_a = E.XAttr E.noSrcSpan (E.XName E.noSrcSpan "id") (mk_str_lit k)
  in E.XTag E.noSrcSpan nm [cl_a, id_a] Nothing

{- | Make Html span element for tagged Exp.

>>> let e = tag_exp "c1" (mk_int_lit 0)
>>> putStr $ E.prettyPrint (tag_to_span (E.Paren E.noSrcSpan e))
<span class = "numeric-literal" id = "c1" >0</span >
-}
tag_to_span :: Exp -> Exp
tag_to_span =
  let f k e = mk_span_id k [e]
  in at_tagged f id

-- | Variant of 'tag_exp' that derives the the tag name using a 'State' counter.
tag_exp_auto :: Exp -> T.State Int Exp
tag_exp_auto e = do
  i <- T.get
  let k = 'c' : show i
  T.put (i + 1)
  return (tag_exp k e)

span_exp_auto :: Exp -> T.State Int Exp
span_exp_auto e = do
  i <- T.get
  let k = 'c' : show i
  T.put (i + 1)
  return (mk_span_id k [e])

-- | Apply /f/ at numeric literals, else /g/.
at_num_lit :: (Exp -> t) -> (Exp -> t) -> Exp -> t
at_num_lit f g e =
  case e of
    E.Lit _ (E.Int _ _ _) -> f e
    E.Lit _ (E.Frac _ _ _) -> f e
    _ -> g e

-- | 'at_num_lit' of 'tag_exp_auto'.
tag_num_lit :: Exp -> T.State Int Exp
tag_num_lit = at_num_lit tag_exp_auto return

-- | 'at_num_lit' of 'tag_exp_auto'.
span_num_lit :: Exp -> T.State Int Exp
span_num_lit = at_num_lit span_exp_auto return

type Parser r = String -> E.ParseResult r
type Rw t m a = t -> m a
type Rw_st t a = t -> T.State Int a
type Tr = String -> String
type Tr_m m = String -> m String
type Rw_Opt = (E.PPLayout, Int)

-- | Parse 'String' using 'Parser' and apply 'Rw'.
apply_rw :: (Monad m, E.Pretty a) => Rw_Opt -> Parser t -> Rw t m a -> Tr_m m
apply_rw (l, w) p f s = do
  let m = E.defaultMode {E.layout = l}
      r = E.fromParseResult (p s)
      sty = E.Style {E.mode = E.PageMode, E.lineLength = w, E.ribbonsPerLine = 1}
  r' <- f r
  return (E.prettyPrintStyleMode sty m r')

apply_rw_pure :: E.Pretty a => Rw_Opt -> Parser t -> (t -> a) -> Tr
apply_rw_pure o p f = T.runIdentity . apply_rw o p (return . f)

apply_rw_st :: E.Pretty a => Rw_Opt -> Parser t -> Rw_st t a -> Tr
apply_rw_st o p f = flip T.evalState 1 . apply_rw o p f

{- | Rewrite 'Exp'.

>>> putStr $ exp_rw "sinOsc ar 440 0 * 0.1"
sinOsc ar (tag "c1" 440) (tag "c2" 0) * tag "c3" 0.1

>>> putStr $ exp_rw "let o = sinOsc ar (midiCPS 65.00) 0.00\n    a = dbAmp (-12.00)\nin o * a"
let o = sinOsc ar (midiCPS (tag "c1" 65.0)) (tag "c2" 0.0)
    a = dbAmp (-tag "c3" 12.0)
  in o * a
-}
exp_rw :: String -> String
exp_rw =
  let f = G.everywhereM (G.mkM tag_num_lit)
  in apply_rw_st (E.PPOffsideRule, 80) E.parseExp f -- PPNoLayout

-- | Rewrite 'Module'.
module_rw :: String -> String
module_rw =
  let f = G.everywhereM (G.mkM tag_num_lit)
  in apply_rw_st (E.PPOffsideRule, 80) E.parseModule f -- PPNoLayout

-- | Inverse of 'exp_rw'.
exp_un_rw :: String -> String
exp_un_rw =
  let f = G.everywhere (G.mkT untag_exp)
  in apply_rw_pure (E.PPOffsideRule, 80) E.parseExp f -- PPNoLayout

-- | 'Rw_Opt' for html.  The /span/ code generates long lines...
rw_html_opt :: Rw_Opt
rw_html_opt = (E.PPOffsideRule, 640)

{- | Transform re-written form to @HTML@.

>>> let e = "sinOsc ar 440 0 * 0.1"
>>> putStr $ exp_rw_html (exp_rw e)
sinOsc ar <span class = "numeric-literal" id = "c1" >440</span > <span class = "numeric-literal" id = "c2" >0</span > * <span class = "numeric-literal" id = "c3" >0.1</span >
-}
exp_rw_html :: String -> String
exp_rw_html =
  let f = G.everywhere' (G.mkT tag_to_span)
  in apply_rw_pure rw_html_opt E.parseExp f

-- | 'Module' variant of 'exp_rw_html'.
module_rw_html :: String -> String
module_rw_html =
  let f = G.everywhere' (G.mkT tag_to_span)
  in apply_rw_pure rw_html_opt E.parseModule f

{- | Translate Haskell expression to Html

>>> putStr $ exp_html "sinOsc ar 440 0 * 0.1"
sinOsc ar <span class = "numeric-literal" id = "c1" >440</span > <span class = "numeric-literal" id = "c2" >0</span > * <span class = "numeric-literal" id = "c3" >0.1</span >
-}
exp_html :: String -> String
exp_html =
  let f = G.everywhereM (G.mkM span_num_lit)
  in apply_rw_st rw_html_opt E.parseExp f

{- | Translate Haskell module to Html

>>> putStr $ module_html "o = sinOsc AR 440 0 * 0.1;main = audition (out 0 o)"
o = sinOsc AR <span class = "numeric-literal" id = "c1" >440</span > <span class = "numeric-literal" id = "c2" >0</span > * <span class = "numeric-literal" id = "c3" >0.1</span >
main = audition (out <span class = "numeric-literal" id = "c4" >0</span > o)
-}
module_html :: String -> String
module_html =
  let f = G.everywhereM (G.mkM span_num_lit)
  in apply_rw_st rw_html_opt E.parseModule f

{- | Embed Html fragment in document structure given directory of Js and Css files.

>>> let e = "sinOsc AR 440 0 * 0.1"
>>> putStr $ html_framework "/home/rohan/sw/hosc-utils" (exp_html e)
<!DOCTYPE html>
<html>
 <head>
  <script src="/home/rohan/sw/hosc-utils/js/json-ws.04.js"></script>
  <link rel="stylesheet" href="/home/rohan/sw/hosc-utils/css/json-ws.04.css" />
 </head>
 <body>
  <pre>
   <code>
sinOsc AR <span class = "numeric-literal" id = "c1" >440</span > <span class = "numeric-literal" id = "c2" >0</span > * <span class = "numeric-literal" id = "c3" >0.1</span >
   </code>
  </pre>
  <p id="sent">[]</p>
 </body>
</html>
-}
html_framework :: String -> String -> String
html_framework d m =
  unlines
    [ "<!DOCTYPE html>"
    , "<html>"
    , " <head>"
    , "  <script src=\"" ++ d ++ "/js/json-ws.04.js\"></script>"
    , "  <link rel=\"stylesheet\" href=\"" ++ d ++ "/css/json-ws.04.css\" />"
    , " </head>"
    , " <body>"
    , "  <pre>"
    , "   <code>"
    , m
    , "   </code>"
    , "  </pre>"
    , "  <p id=\"sent\">[]</p>"
    , " </body>"
    , "</html>"
    ]
