-- | Rewriter for @psynth@ directives and related functions.
module Sound.Sc3.Rw.Psynth where

import Data.Char {- base -}
import Data.List {- base -}
import System.Environment {- base -}

import Data.Functor.Identity {- transformers -}

import qualified Text.Parsec as P {- parsec -}
import qualified Text.Parsec.Language as P {- parsec -}
import qualified Text.Parsec.Token as P {- parsec -}

import Sound.Sc3.Server.Param {- hsc3 -}

-- * Types

-- | Name of 'SynthDef' and associated 'Param'.
type Psynth = (String, Param)

-- * Pretty printer

{- | Printer for control and trigger parameters.

>>> map ps_param1_pp [("freq",440),("t_gate",1)]
["freq = control KR \"freq\" 440.0","t_gate = tr_control \"t_gate\" 1.0"]
-}
ps_param1_pp :: Param1 -> String
ps_param1_pp (nm, def) =
  let fn =
        if "t_" `isPrefixOf` nm
          then "tr_control"
          else "control KR"
  in concat [nm, " = ", fn, " \"", nm, "\" ", show def]

add_braces :: String -> String
add_braces s = concat ["{", s, "}"]

ps_param_pp :: Char -> Param -> String
ps_param_pp c = intercalate [c] . map ps_param1_pp

uparam_pp :: Param -> String
uparam_pp p = "let " ++ ps_param_pp ';' p

psynth_pp :: Psynth -> String
psynth_pp (nm, pp) = concat [nm, " = synthdef \"", nm, "\" (let ", add_braces (ps_param_pp ';' pp), " in"]

-- * Parser

type P a = P.ParsecT String () Identity a

promote :: Either Integer Double -> Double
promote = either fromIntegral id

assign :: P Param1
assign = do
  lhs <- identifier
  _ <- equals
  rhs <- naturalOrFloat
  return (lhs, promote rhs)

param_list :: P Param
param_list = P.sepBy1 assign comma

uparam :: P Param
uparam = symbol "let" >> symbol "uparam" >> equals >> braces param_list

psynth :: P Psynth
psynth = do
  nm <- identifier
  _ <- equals
  _ <- symbol "psynth"
  pp <- braces param_list
  _ <- symbol "where"
  return (nm, pp)

{- | Parse 'Psynth' pre-amble.

>>> parse_psynth "gr = psynth {freq = 440,phase = 0,amp = 0.1,loc = 0} where"
("gr",[("freq",440.0),("phase",0.0),("amp",0.1),("loc",0.0)])
-}
parse_psynth :: String -> Psynth
parse_psynth s =
  case P.parse psynth "parse_psynth" s of
    Left e -> error (show e)
    Right r -> r

{- | Rewrite 'Psynth' pre-amble.

>>> rewrite_psynth "gr = psynth {freq = 440,phase = 0,amp = 0.1,loc = 0} where"
"gr = synthdef \"gr\" (let {freq = control KR \"freq\" 440.0;phase = control KR \"phase\" 0.0;amp = control KR \"amp\" 0.1;loc = control KR \"loc\" 0.0} in"
-}
rewrite_psynth :: String -> String
rewrite_psynth = psynth_pp . parse_psynth

parse_param_list :: String -> Param
parse_param_list s =
  case P.parse param_list "parse_param_list" s of
    Left e -> error (show e)
    Right r -> r

{- | Rewrite plain 'Param' list, ie. Sc3 argument list.

>>> rewrite_param_list '\n' "freq=440,amp=0.1,t_gate=1"
"freq = control KR \"freq\" 440.0\namp = control KR \"amp\" 0.1\nt_gate = tr_control \"t_gate\" 1.0"

>>> rewrite_param_list '\n' "freq = 440, amp = 0.1, t_gate = 1"
"freq = control KR \"freq\" 440.0\namp = control KR \"amp\" 0.1\nt_gate = tr_control \"t_gate\" 1.0"
-}
rewrite_param_list :: Char -> String -> String
rewrite_param_list c = ps_param_pp c . parse_param_list

parse_uparam :: String -> Param
parse_uparam s =
  case P.parse uparam "parse_uparam" s of
    Left e -> error (show e)
    Right r -> r

rewrite_uparam :: String -> String
rewrite_uparam = uparam_pp . parse_uparam

lexer :: P.GenTokenParser String u Identity
lexer = P.makeTokenParser P.haskellDef

braces :: P a -> P a
braces = P.braces lexer

identifier :: P String
identifier = P.identifier lexer

symbol :: String -> P String
symbol = P.symbol lexer

naturalOrFloat :: P (Either Integer Double)
naturalOrFloat = P.naturalOrFloat lexer

equals :: P String
equals = P.lexeme lexer (P.string "=")

comma :: P String
comma = P.comma lexer

semi :: P String
semi = P.semi lexer

-- * Re-write processor

begins_psynth :: String -> Bool
begins_psynth = isInfixOf " = psynth {"

ends_psynth :: String -> Bool
ends_psynth s =
  case s of
    [] -> True
    c : _ -> not (isSpace c)

psynth_rewrite :: [String] -> [String]
psynth_rewrite l =
  case break begins_psynth l of
    ([], rhs) -> rhs
    (lhs, []) -> lhs
    (lhs, p : rhs) -> case break ends_psynth rhs of
      (lhs', rhs') ->
        concat
          [ lhs
          , [rewrite_psynth p]
          , lhs'
          , [" )"]
          , psynth_rewrite rhs'
          ]

-- | Arguments as required by @ghc -F -pgmF@.
psynth_rewrite_ghcF :: IO ()
psynth_rewrite_ghcF = do
  a <- getArgs
  case a of
    [_, i_fn, o_fn] -> do
      i <- readFile i_fn
      let f = unlines . psynth_rewrite . lines
      writeFile o_fn (f i)
    _ -> error "initial-file input-file output-file"

{- | Rewrite uparam pre-amble.

>>> uparam_rewrite "    let uparam = {amp = 0.1, freq = 129.897, rise = 0.1, fall = 0.5}"
"    let amp = control KR \"amp\" 0.1;freq = control KR \"freq\" 129.897;rise = control KR \"rise\" 0.1;fall = control KR \"fall\" 0.5"
-}
uparam_rewrite :: String -> String
uparam_rewrite s =
  if "let uparam = {" `isInfixOf` s
    then case span isSpace s of
      ([], _) -> error "uparam_rewrite"
      (lhs, rhs) -> lhs ++ rewrite_uparam rhs
    else s

-- | Arguments as required by @ghc -F -pgmF@.
uparam_rewrite_ghcF :: IO ()
uparam_rewrite_ghcF = do
  a <- getArgs
  case a of
    [_, i_fn, o_fn] -> do
      i <- readFile i_fn
      let f = unlines . map uparam_rewrite . lines
      writeFile o_fn (f i)
    _ -> error "initial-file input-file output-file"
