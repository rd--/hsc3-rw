Composition of graphs, user specified identifiers
=================================================

> import Sound.SC3 {- hsc3 -}
> import Sound.SC3.UGen.Protect {- hsc3-rw -}

`uprotect` transforms a UGen graph by editing existing identifiers.
There is a halting function to delimit the scope of the re-writing.
The variant `uprotect_all` has `const False` as the halting rule.

> g_01 = whiteNoise 'α' AR
> k_01 = UId (resolveID 'α')
> g_02 = uprotect_all 'β' g_01
> k_02 = UId (resolveID ('β',resolveID 'α'))

    >> map ugenIds [g_01,g_02] == [[k_01],[k_02]]

`uclone` is a shorthand for writing out an `mce` node
with multiple copies of a unit generator graph.  That
is, it traverses the UGen graph and modifies each user
supplied identifier.  `uclone` is a variant form
of `uprotect`.

> g_03 = uclone_all 'α' 2 g_01

    >> map ugenIds [g_01,g_03] == [[UId 1439603815],[UId 3747863487,UId 1883699439]]

The identifier input to `uclone` allows multiple
instances to generate distinct graphs.

> g_04 = uclone_all 'α' 2 g_01 + uclone_all 'β' 2 g_01

    >> ugenIds g_04 == [NoId,UId 3747863487,UId 2530953129,NoId,UId 1883699439,UId 651621932]

`ucompose` is left to right composition of `UGen`
processing functions where at each stage user
identifiers are re-written.

> g_05 =
>   let f i = allpassN i 0.050 (rand 'α' 0 0.05) 1
>   in ucompose (const False) 'α' [f,f,f,f] (dust 'α' AR 10)

    >> synthstat_wr g_05
    ...
    unit generator set        : AllpassN×4 Dust×1 Rand×4
    ...

`useq` is a variant that replicates the same function
for composition.

> g_06 =
>   let f i = allpassN i 0.050 (rand 'α' 0 0.05) 1
>   in useq_all 'α' 4 f (dust 'α' AR 10)

    >> g_05 == g_06
