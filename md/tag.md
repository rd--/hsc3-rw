# RW.TAG

A rewriting system for building real-time interactive editors for
SuperCollider instruments.  It works by re-writing the _textual
notation_ of the graph and making the numeric constants in the
notation directly editable.

The first source text translation stage tags all numeric literals.
The function `exp_rw` rewrites:

    sinOsc AR 440 0 * 0.1

as:

    sinOsc AR (tag "c1" 440) (tag "c2" 0) * tag "c3" 0.1

In this instance all such literals are of type `UGen` but this need
not be the case.

For the standard numerical types `tag` has no effect, for `UGen`
literals however it lifts the `Constant` value indicated by the
literal to a `Control` value.

The second source text translation stage generates an
[HTML](http://www.w3.org/TR/html5/) representation where tagged
literals are indicated as `span` elements with an appropriate `id`
attribute.  The function `exp_rw_html` rewrites:

    tag "c2" 0

as:

    <span class = "numeric-literal" id = "c2" >0</span >

Tagged values where the name is not in the set of control names of the
translated source can be untagged.

The translated HTML text can have controls identified by a CSS
instruction and be directly manipulated using a javascript
library.

# Rationale

The transformation automatically derives an interactive editor for a
real-time synthesiser that directly reflects the structure of the
graph as it was written down.

The translation renders the interactive editor in HTML since that is
straight-forward and widely accessible, however it would be more
useful if this worked directly in the editor (ie. in emacs).
