# hsc3-rw

Sub-commands are:
[hash-at](?t=hsc3-rw&e=md/hash-at.md),
[hash-paren](?t=hsc3-rw&e=md/hash-paren.md),
[id-clear](?t=hsc3-rw&e=md/id-clear.md),
[id-rewrite](?t=hsc3-rw&e=md/id-rewrite.md),
[psynth](?t=hsc3-rw&e=md/psynth.md),
[uparam](?t=hsc3-rw&e=md/uparam.md)
