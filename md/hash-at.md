# hash-at (June, 2014)

Rewrite `#@` character sequences in a text with a `hash` of the line
and column numbers indicating the location of the sequence in the text.

The input text:

    let o = sinOsc AR (rand #@ 220 440) (rand2 #@ pi) * 0.1

is re-written as:

    let o = sinOsc AR (rand (hash "1:24") 220 440) (rand2 (hash "1:43") pi) * 0.1

# Rationale

This idea is essentially misguided.

It is a variant of the ['x'](?t=hsc3-rw&e=md/id-rewrite.md) re-writing
process, operating as a [ghc](http://haskell.org/ghc) pre-processor
instead of as an [emacs](http://www.gnu.org/software/emacs/) editor
command.

There are only very few rather obscure reasons for ever using this
approach.
