# uparam (June, 2014)

The `uparam` notation allows eliding control names and constructors.

The notation is:

~~~~
waveset =
    let uparam = {bus=0,bufnum=0,start=0,end=0,rate=1,sustain=1,amp=0.2}
        rs = bufRateScale KR bufnum * rate
        ph = phasor AR 0 rs 0 (end - start) 0 + start
        e_data = Envelope [amp, amp, 0] [sustain, 0] [EnvLin] Nothing Nothing
        e_ugen = envGen AR 1 1 0 1 RemoveSynth e_data
    in offsetOut bus (bufRdL 1 AR bufnum ph Loop * e_ugen)
~~~~

The re-written form is:

~~~~
waveset =
    let bus = control KR "bus" 0.0
        bufnum = control KR "bufnum" 0.0
        start = control KR "start" 0.0
        end = control KR "end" 0.0
        rate = control KR "rate" 1.0
        sustain = control KR "sustain" 1.0
        amp = control KR "amp" 0.2
        rs = bufRateScale KR bufnum * rate
        ph = phasor AR 0 rs 0 (end - start) 0 + start
        e_data = Envelope [amp, amp, 0] [sustain, 0] [EnvLin] Nothing Nothing
        e_ugen = envGen AR 1 1 0 1 RemoveSynth e_data
    in offsetOut bus (bufRdL 1 AR bufnum ph Loop * e_ugen)
~~~~

There is also an expander that can be called interactively from a text editor.

~~~~
$ echo freq=440,amp=0.1 | hsc3-rw uparam expand
freq = control KR "freq" 440.0
amp = control KR "amp" 0.1
$
~~~~

# Rationale

UGen graphs that have many controls are simplest to write if the name
of the control and the name of the variable representing the control
are equal.

In haskell this can be verbose to write out, the name must be written
twice.

The [SuperCollider](http://audiosynth.com) language allows writing
controls as _parameters_ to the _framing_ function that constructs the
UGen graph.

~~~~
{arg f = 440, a = 0.1
;Out.ar(0,SinOsc.ar(f,0) * a)}
~~~~

The [rsc3](?t=rsc3) bindings provide the related _macro_ `letc`.

~~~~
(letc ((f 440) (a 0.1))
  (out 0 (mul (sin-osc ar f 0) a))))
~~~~
