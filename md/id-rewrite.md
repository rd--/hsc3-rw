# id-rewrite

Rewrite all single character identifiers, of the form `'x'`, within a
text, replacing each with a letter from the greek alphabet.

The letters of the initial text are completely ignored, and the
letters introduced are in ascending sequence, so for instance the
string `"'x' 'y' 'z'"` would be re-written `"'α' 'β' 'γ'"`.

The [hsc3](?t=hsc3) [emacs](http://www.gnu.org/software/emacs/) mode
([hsc3.el](?t=hsc3&e=emacs/hsc3.el)) has two functions that run this
operation, one to act on the whole of the buffer, the other to act on
the selected region.

# Rationale

Ordinarily the only rule for writing identifiers for non-determinstic
UGens in hsc3 graphs is that they be distinct.  A single character
provides enough identifiers for most graphs, and the emacs syntax
colouring makes character identifiers easy to see.  (There are rarely
any other single characters in hsc3 files, almost never within UGen
graph functions.)

This command, and the emacs bindings for it, mean that when writing
graphs there is no need to consider the identifiers, placeholders can
be written and then replaced.  If a graph is written as:

~~~~
why_supercollider =
    let r = resonz (dust 'a' AR 0.2 * 50) (rand 'a' 200 3200) 0.003
        s = mix (uclone 'a' 10 r)
        z = delayN s 0.048 0.048
        c = combL z 0.1 (lfNoise1 'a' KR (rand 'a' 0 0.1) * 0.04 + 0.05) 15
        y = mix (uclone 'a' 7 c)
        f i = allpassN i 0.05 (randN 2 'a' 0 0.05) 1
        x = useq 'a' 4 f y
    in out 0 (s + 0.2 * x)
~~~~

the result of running `hsc3-id-rewrite-region` is:

~~~~
why_supercollider =
    let r = resonz (dust 'α' AR 0.2 * 50) (rand 'β' 200 3200) 0.003
        s = mix (uclone 'γ' 10 r)
        z = delayN s 0.048 0.048
        c = combL z 0.1 (lfNoise1 'δ' KR (rand 'ε' 0 0.1) * 0.04 + 0.05) 15
        y = mix (uclone 'ζ' 7 c)
        f i = allpassN i 0.05 (randN 2 'η' 0 0.05) 1
        x = useq 'θ' 4 f y
    in out 0 (s + 0.2 * x)
~~~~
