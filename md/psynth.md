# psynth (June, 2014)

The `psynth` notation allows eliding `Synthdef` and control names and constructors.

It uses the [uparam](?t=hsc3-rw&e=md/uparam.md) notation for writing controls.

The notation is:

~~~~
sinuscell = psynth {bus=0,pan=0,amp=0.5,freq=400,atk=1,sus=0.2,rel=1} where
    let e_d = envLinen atk sus rel amp
        e = envGen KR 1 0.1 0 1 RemoveSynth e_d
        s = sinOsc AR freq 0
    in out bus (pan2 s pan e)
~~~~

The re-written form is:

~~~~
sinuscell =
  synthdef "sinuscell" (let
    {bus = control KR "bus" 0.0
    ;pan = control KR "pan" 0.0
    ;amp = control KR "amp" 0.5
    ;freq = control KR "freq" 400.0
    ;atk = control KR "atk" 1.0
    ;sus = control KR "sus" 0.2
    ;rel = control KR "rel" 1.0}
  in let e_d = envLinen atk sus rel amp
         e = envGen KR 1 0.1 0 1 RemoveSynth e_d
         s = sinOsc AR freq 0
     in out bus (pan2 s pan e))
~~~~

# Rationale

This is a simple extension of the `uparam` rules to also define and
name the `SynthDef` associated with the graph.
