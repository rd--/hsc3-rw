hsc3-rw - hsc3 re-writing
-------------------------

[hsc3-rw][hsc3-rw] provides re-writing functions for use with
[hsc3][hsc3].  Modules are at `Sound.SC3.RW`.

- `CharId` re-writes ['x'](?t=hsc3-rw&e=md/id-rewrite.md) identifiers (March, 2013)
- `HashAt` re-writes [#@](?t=hsc3-rw&e=md/hash-at.md) notation (June, 2014)
- `HashParen` re-writes [#()](?t=hsc3-rw&e=md/hash-paren.md) notation (March, 2013)
- `PSynth` re-writes both [uparam](?t=hsc3-rw&e=md/uparam.md)
                      and [psynth](?t=hsc3-rw&e=md/psynth.md) notations (June, 2014)
- `Tag` [tag](?t=hsc3-rw&e=md/tag.md)s numeric literals with identifiers (February, 2013)

## cli

[hsc3-rw](?t=hsc3-rw&e=md/rw.md)

[hsc3]: http://rohandrape.net/?t=hsc3
[hsc3-rw]: http://rohandrape.net/?t=hsc3-rw

© [rohan drape][rd], 2013-2025, [gpl][gpl].

[rd]: http://rohandrape.net/
[gpl]: http://gnu.org/copyleft/

* * *

```
$ make doctest
Examples: 70  Tried: 70  Errors: 0  Failures: 0
$
```
