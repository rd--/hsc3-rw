import System.Environment {- base -}

import Sound.Sc3.Rw.CharId {- hsc3-rw -}
import Sound.Sc3.Rw.HashAt {- hsc3-rw -}
import Sound.Sc3.Rw.HashParen {- hsc3-rw -}
import Sound.Sc3.Rw.Psynth {- hsc3-rw -}

usage :: [String]
usage = ["hsc3-rw cmd",""," hash-at"," hash-paren"," id-clear"," id-rewrite"," psynth"," uparam"]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["hash-at"] -> ha_rewrite_ghcF
    ["hash-paren"] -> hp_rewrite_ghcF
    ["id-clear"] -> interact hsc3_id_clear
    ["id-rewrite"] -> interact hsc3_id_rewrite
    ["psynth"] -> psynth_rewrite_ghcF
    ["uparam"] -> uparam_rewrite_ghcF
    ["uparam","expand"] -> interact (rewrite_param_list '\n')
    _ -> putStrLn (unlines usage)
