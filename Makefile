all:
	echo "hsc3-rw"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hsc3-rw

indent:
	fourmolu -i Sound

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
